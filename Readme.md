# FoundryVTT - Acaria Empire Dices

This module comes on top of [Dice So Nice!](https://gitlab.com/riccisi/foundryvtt-dice-so-nice) module.


## Usage Instructions

There are no particular instructions for use. Once the module is enabled, 3D animation will be displayed each time dice is rolled on foundry.

![Preview](/demo_ae_dices.gif?raw=true)


import {DiceLoader} from './dices/DiceLoader.js';
import {AEDiceMessage} from './dices/DiceMessage.js';

import { AETokensHud } from './gui/TokensHud.js';
import { AEDiceStatistics } from './dices/DiceStatistics.js';
import { BalanceTokenDef, ComplicationTokenDef, RiskDiceDef, SuccessDiceDef } from './dices/DiceDefinitions.js';
import { computeAERollResult } from './commands/RollResult.js';
import { AEDangerosityHud } from './gui/DangerosityHud.js';

/**
 * Registers the exposed settings for the various 3D dice options.
 */
Hooks.once('init', () => {

	CONFIG.Dice.terms["s"] = SuccessDiceDef;
	CONFIG.Dice.terms["r"] = RiskDiceDef;
	CONFIG.Dice.terms["b"] = BalanceTokenDef;
	CONFIG.Dice.terms["c"] = ComplicationTokenDef;
 	// CONFIG.debug.hooks = true;
});


/**
 * Load structure
 */
Hooks.once('acariaEmpireReady', async () => {
	init.systemOK = true;
	loadModule();
});

/**
 * Triggered when dice-so-nice has been loaded
 */
Hooks.once('diceSoNiceInit', (dice3d) => {
	init.diceSoNice = dice3d;
	loadModule();
});

const init = {
	systemOK: false,
	diseSoNice: null
}
const loadModule = async () => {

	if( !init.diceSoNice || !init.systemOK ) { return; }

    game.aedice = new AEDice();
	await game.aedice.huds.tokens.render(true);
	await game.aedice.huds.dangerosity.render(true);
	game.aedice.diceLoader = new DiceLoader(init.diceSoNice);
}

// Intercept current selection modifications and update gui
//--------------------------------------------------

Hooks.on('controlToken', (token, controlled) => {
	
	if( canvas.ready && game.aedice ) {

		let actorToDisplay = canvas.tokens.controlled.length > 0 ? canvas.tokens.controlled[0].actor : null;
		game.aedice.huds.tokens.changeCurrentSelection(actorToDisplay);
	}
});

Hooks.on("updateActor", (actor, data, options, user) => {
	game.aedice?.huds.tokens.characterTokensHasBeenUpdated(actor);
});
  
Hooks.on("updateActorEquipment", (actor, data, options, user) => {
	game.aedice?.huds.tokens.characterTokensHasBeenUpdated(actor);
});
  
Hooks.on("updateCustomCardsContent", (cards, options, user) => {
	game.aedice?.huds.tokens.thereAreCardsMovements(cards);
});

Hooks.on('canvasDraw', (canvas) => {
	console.log("canvasDraw", canvas);
	game.aedice?.huds.dangerosity.changeCurrentScene();
});
	
Hooks.on('updateScene', (scene) => {
	game.aedice?.huds.dangerosity.aSceneHasBeenUpdated(scene);
});


/**
 * Main class to handle AE 3D Dice animations.
 */
export class AEDice {

	constructor() {
		this.huds = {
			tokens: new AETokensHud(),
			dangerosity: new AEDangerosityHud()
		}

		this.huds.tokens = new AETokensHud();
		this._statsgui = new AEDiceStatistics();
        this._msgBuilder = new AEDiceMessage();
	}

	/**
	 * Allow to update character tokens before rolling dices
	 * @param {RollDetail} context : See interaction/RollDetail.js. Give all nneded informations for the roll
	 */
	async rollDice(context) {
		const masteryDice = context.dice.masteryDice;
		const riskDice = context.dice.riskDice;
		const replacedDiceFace = context.dice.replacedDiceFace;
		const tokenId = context.actor.tokenId;

		// Before rolling dices, update character tokens so that they can be token into account during roll.
		let actor = game.aesystem.actorTools.getActorOrTokenActor({tokenId: tokenId});
		if( ! actor && !game.user.isGM )  {
			actor = game.user.character;
		}
		if( ! actor )  {
			return ui.notifications.warn(game.i18n.localize("AEDICES.warnNoActorSpecified"));
		}

		// Never have more replaced dice than the amount rolled
		replacedDiceFace.length = Math.min(masteryDice, replacedDiceFace.length);
		const rolledClassicDice = Math.max(0, masteryDice - replacedDiceFace.length);
		let formula = rolledClassicDice + 'ds';
		if( riskDice > 0 ) {
			formula += ' + ' + riskDice + 'dr';
		}

		let myRoll = new Roll(formula);
		await myRoll.evaluate();
		
		const rollData = await computeAERollResult(actor, myRoll, context);

		// Displaying roll while adding used tokens and replaceDice
		// --------------------------------------
		const plusTerm = new OperatorTerm({operator: "+"});
		plusTerm._evaluated = true;

		const tokens = rollData.context.roll;
		const allTerms = myRoll.terms;

		// - Adding card balance tokens
		if( tokens.cardBalance != 0 ) {
			allTerms.push( plusTerm );
			const face = tokens.cardBalance > 0 ? 2 : 1;
			const results = [];
			for( let i = 0; i < Math.abs(tokens.cardBalance); i++ ) {
				results.push( {result: face, active: true} );
			}
			allTerms.push( new BalanceTokenDef({number: results.length, results}) );
		}

		// - Adding complication tokens
		const complication = context.roll.complication;
		if( complication != 0 ) {
			allTerms.push( plusTerm );
			const face = complication > 0 ? 1 : 2;
			const results = [];
			for( let i = 0; i < Math.abs(complication); i++ ) {
				results.push( {result: face, active: true} );
			}
			allTerms.push( new ComplicationTokenDef({number: results.length, results}) );
		}

		// - Adding success dice with fixed value (mainly from event cards)
		if( replacedDiceFace.length > 0 ) {
			allTerms.push( plusTerm );
			const results = replacedDiceFace.map( f => {
				return { result: f, active: true };
			});;
			allTerms.push( new SuccessDiceDef({number: results.length, results}) );
		}

		const completeRoll = Roll.fromTerms(allTerms);

		// Call to dice-so-nice
		game.dice3d.showForRoll(completeRoll, game.user, true);
	}

	async displayStatisticsOnLogs() {
		this._statsgui.reset();
		this._statsgui.render(true);
	}

	async displayStatMessage( {baseScore=0, cardBalance=0, nbMasteryDice=3, nbRiskDice=0, spiritTrouble="", selfTalk=false} = {}) {
		return this._statsgui.displayStatMessage([baseScore], [cardBalance], [nbMasteryDice], [nbRiskDice], {spiritTrouble, selfTalk});
	}
		
}



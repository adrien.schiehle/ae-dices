export const translationKey = (base, checkedValue, predicate = (_val) => _val > 1, suffixIfTrue = "many", suffixOtherwise = "one") => {
    return base + "." + (predicate(checkedValue) ? suffixIfTrue : suffixOtherwise);
}

export const translate = (value, substitutions = {}) => {
    let result = game.i18n.localize(value);
    Object.entries(substitutions).forEach( ([key, value]) => {
        result = result.replace(key, value);
    });
    return result;
}

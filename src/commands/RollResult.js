import {AEDiceInterpretor} from '../dices/DiceInterpretor.js';


/**
 * Once the dice have been displayed. See the result, create a chatMessage and update actor if needed.
 * @returns {object} computed roll data
 */
export const computeAERollResult = async (actor, roll, context) => {

    // Build rollData from roll, current difficulty and context
	const scoreBase = context.roll.scoreBase;
	const riskDice = context.dice.riskDice;
	const replacedDiceFace = context.dice.replacedDiceFace;
	const difficulty = {scoreBase: scoreBase, risk: riskDice};
    const rollData = game.aedice.diceLoader.retrieveRollDataFromMessage(roll, difficulty, replacedDiceFace);
	rollData.context = context;

    // Compute results
	let result = await AEDiceInterpretor.interpretRoll(actor, rollData);
    await game.aedice._msgBuilder.displayRollResult(result, actor);

	// Self damage and heal
	const healthChange = result.action.healthChange;
	if( healthChange.damage > 0 ) {
		await actor.inflictDamage(healthChange.damage);
	} else if( healthChange.damage < 0 ) {
		await actor.healDamage(0 - healthChange.damage);
	}

	if( healthChange.fatigue > 0 ) {
		await actor.inflictFatigue(healthChange.fatigue);
	} else if( healthChange.fatigue < 0 ) {
		await actor.healFatigue(0 - healthChange.fatigue);
	}
	

	// Action duration modifier
	if( result.action.duration ) {
		await actor.spendTime(result.action.duration);
	}

    // Draw cards if needed
	for( let cardData of result.cardsToDraw ) {
		await game.aesystem.actorTools.letUserDrawEvents(cardData.user_id, cardData.nb);
	}

	// Alter circle if needed
	const arcane = result.context?.arcane;
	if(arcane?.deleteCircle) {
		await result.actor.arcane.deleteCurrentCircle();
	}

	// Resolve communion
	const communion = result.context?.communion;
	if(communion) { 
		await result.actor.divine.alterCommunion(communion);
	}

	// Upgrade melody if needed
	const melody = result.context?.melody;
	if( melody?.upgrade ) { 
		await result.actor.songs.alterCurrentPerformance(melody.melodyKey, melody.melodyLevel, melody.dissonance);
	}
	if( melody?.stop ) { 
		await result.actor.songs.stopCurrentPerformance();
	}

	// Add spell to combat tracker
	const spellData = result.context?.spellData;
	if( spellData ) {
		actor.trackNewSpellEffect(spellData);
	}

	return rollData;
}

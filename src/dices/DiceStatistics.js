import { CLASSICDICE_FACES, RISKDICE_FACES } from './DiceInterpretor.js';
import { translationKey, translate } from '../tools/Translator.js';

const CARDS_COLOR_FOR_OPPONENT = [
    'rgb(180, 180, 180)',
    'rgb(180, 150, 150)',
    'rgb(180, 120, 120)',
    'rgb(180, 90, 90)',
    'rgb(180, 60, 60)',
    'rgb(180, 30, 30)',
    'rgb(180, 0, 0)'
];

const CARDS_COLOR_FOR_ACTOR = [
    'rgb(180, 180, 180)',
    'rgb(150, 180, 150)',
    'rgb(120, 180, 120)',
    'rgb(90, 180, 90)',
    'rgb(60, 180, 60)',
    'rgb(30, 180, 30)',
    'rgb(0, 180, 0)'
];

const COLORS_FOR_SUCCESSES = {
    disaster: 'rgb(0, 0, 0)',
    epicFail: 'rgb(61, 61, 61)',
    failure: 'rgb(109, 109, 109)',
    success: 'rgb(96, 126, 106)',
    critical: 'rgb(44, 131, 73)',
    incredible: 'rgb(0, 172, 57)'
};

const SELECT_ICON = {
    notSelected: 'modules/ae-dices/resources/stats/not-selected.png',
    selected: 'modules/ae-dices/resources/stats/selected.png'
};


const successStat = (caseAmount, combinationsAmount, successKey ) => {

    const value = ((caseAmount/combinationsAmount) * 100.0).toFixed(0);
    const successLabel = game.i18n.localize('AEDICES.chat.result.' + successKey );
    const tooltip = game.i18n.localize('AEDICES.stats.success.tooltip')
                        .replace('STATS', value)
                        .replace('SUCCESS', successLabel);

    const color = COLORS_FOR_SUCCESSES[successKey];
    const style = 'flex:' + value + '; background-color:' + color + ';';
    return {
        caseAmount: caseAmount,
        displayed: value > 0,
        value: value,
        tooltip: tooltip,
        style: style
    };
}

const cardStat = (caseAmount, combinationsAmount, nbCard, color, labelSuffix ) => {

    const value = ((caseAmount/combinationsAmount) * 100.0).toFixed(0);
    const tooltip = game.i18n.localize("AEDICES.stats.event." + labelSuffix + '.tooltip')
                        .replace('STATS', value)
                        .replace('AMOUNT', nbCard);
    const style = 'flex:' + value + '; background-color:' + color + ';';
    return {
        nbCard: nbCard,
        caseAmount: caseAmount,
        displayed: value > 0,
        value: value,
        tooltip: tooltip,
        style: style
    };
}

const parseSelectedTroubleKey = (selectedTrouble) => {
    if(selectedTrouble === "") { return undefined; }
    const splitted = selectedTrouble.split("#");
    const troubleRoot = game.aesystem.allAvailableTroubles().find(t => t.key === splitted[0]);
    return troubleRoot ? troubleRoot[splitted[1]] : undefined;
}

const alterDiceFaces = (baseFaces, trouble) => {
    if( !trouble ) { return baseFaces; }

    const result = [];
    baseFaces.forEach(face => {
        const realFace = {...face};
        if( realFace.depends ) {
            realFace.skulls += trouble.skulls ?? 0;
            realFace.power += trouble.powers ?? 0;
            realFace.success += trouble.success ?? 0;
            if( trouble.events > 0 ) {
                realFace.card += trouble.events;
            } else if( trouble.events < 0 ) {
                realFace.opponentCard -= trouble.events;
            }
            realFace.depends = 0;
        }
        result.push(realFace);
    })
    return result;
}

const buildAllCombinations = (selectedTrouble) => {

    const addDiceToCombinations = (combinations, faces) => {
        const result = [];
        combinations.forEach( base => {
            faces.forEach( face => { 
                result.push({
                    success: base.success + face.success, 
                    skull: base.skull + face.skull, 
                    power: base.power + face.power, 
                    card: base.card + face.card, 
                    opponentCard: base.opponentCard + face.opponentCard,
                    depends: base.depends + face.depends
                });
            });
        });
        return result;
    }
    const trouble = parseSelectedTroubleKey(selectedTrouble);
    const masteryFaces = alterDiceFaces(CLASSICDICE_FACES, trouble); 
    const riskFaces = alterDiceFaces(RISKDICE_FACES, trouble); 

    
    const combination_1_0 = [...masteryFaces];
    const combination_2_0 = addDiceToCombinations(combination_1_0, masteryFaces);
    const combination_3_0 = addDiceToCombinations(combination_2_0, masteryFaces);

    const combination_1_1 = addDiceToCombinations(combination_1_0, riskFaces);
    const combination_2_1 = addDiceToCombinations(combination_2_0, riskFaces);
    const combination_3_1 = addDiceToCombinations(combination_3_0, riskFaces);

    const combination_1_2 = addDiceToCombinations(combination_1_1, riskFaces);
    const combination_2_2 = addDiceToCombinations(combination_2_1, riskFaces);
    const combination_3_2 = addDiceToCombinations(combination_3_1, riskFaces);

    const combination_1_3 = addDiceToCombinations(combination_1_2, riskFaces);
    const combination_2_3 = addDiceToCombinations(combination_2_2, riskFaces);
    const combination_3_3 = addDiceToCombinations(combination_3_2, riskFaces);

    return [
        {mastery: 1, risk: 0, combinations: combination_1_0},
        {mastery: 2, risk: 0, combinations: combination_2_0},
        {mastery: 3, risk: 0, combinations: combination_3_0},

        {mastery: 1, risk: 1, combinations: combination_1_1},
        {mastery: 2, risk: 1, combinations: combination_2_1},
        {mastery: 3, risk: 1, combinations: combination_3_1},

        {mastery: 1, risk: 2, combinations: combination_1_2},
        {mastery: 2, risk: 2, combinations: combination_2_2},
        {mastery: 3, risk: 2, combinations: combination_3_2},

        {mastery: 1, risk: 3, combinations: combination_1_3},
        {mastery: 2, risk: 3, combinations: combination_2_3},
        {mastery: 3, risk: 3, combinations: combination_3_3},
    ];
}

export class AEDiceStatistics extends Application {

    // For stat calculation
    #allCombinations = new Map();
    #allSuccessStats = [];
    #allCardDrawStats = [];

    // For gui
    #selectedTrouble = "";
    #successValues = [];
    #cardBalanceValues = [];
    #masteryValues = [];
    #riskValues = [];

    /** @override */
	static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
              classes: ["acariaempire", "dice-stats", "popup"],
              template: "modules/ae-dices/resources/stats/stats-popup.hbs",
              width: 'auto',
              height: 'auto'
        });
    }

    constructor(options = {}) {
        super(options);

        console.log('AE-DiceStatistics | Initializing...');
        this.reset();
    }

    reset() {
        const everySucess = [-6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6];
        this.#successValues = everySucess.map( t => {
            return {
                value: t,
                selected: t == 0
            };
        });

        const everyCadBalances = [-3, -2, -1, 0, 1, 2, 3];
        this.#cardBalanceValues = everyCadBalances.map( t => {
            return {
                value: t,
                selected: t == 0
            };
        });

        const everyMasteries = [1, 2, 3];
        this.#masteryValues = everyMasteries.map( t => {
            return {
                value: t,
                selected: t == 3
            };
        });

        this.#riskValues = [0, 1, 2, 3].map( t => {
            return {
                value: t,
                selected: t == 0
            };
        });
    }

    get allSuccessValuesSelected() {
        return this.#successValues.findIndex( e => !e.selected ) == -1;
    }

    get allCardBalanceTokensSelected() {
        return this.#cardBalanceValues.findIndex( e => !e.selected ) == -1;
    }

    get allMasteryValuesSelected() {
        return this.#masteryValues.findIndex( e => !e.selected ) == -1;
    }

    get allRiskValuesSelected() {
        return this.#riskValues.findIndex( e => !e.selected ) == -1;
    }

    /*------------------------------------
     * Calculation part
      ------------------------------------*/    
    getCombinations(troubleKey, nbMastery, nbRisk) {
        let combinations = this.#allCombinations.get(troubleKey);
        if( ! combinations ) {
            combinations = buildAllCombinations(troubleKey);
            this.#allCombinations.set(troubleKey, combinations);
        }
        const section = combinations.find( c => c.mastery == nbMastery && c.risk == nbRisk);
        return section ? [...section.combinations] : [];
    }


    getSucessStat(troubleKey, baseScore, mastery, risk) {
        let stat = this.#allSuccessStats.find(s => 
            s.troubleKey === troubleKey && s.baseScore === baseScore && s.mastery === mastery && s.risk === risk
        );
        if(!stat) {
            stat = {troubleKey, baseScore, mastery, risk, ...this.#calculateSuccessProbability(troubleKey, baseScore, mastery, risk)};
            this.#allSuccessStats.push(stat);
        }
        return stat;
    }

    getCardDrawStat(troubleKey, cardBalance, mastery, risk) {
        let stat = this.#allCardDrawStats.find(s => 
            s.troubleKey === troubleKey && s.cardBalance === cardBalance && s.mastery === mastery && s.risk === risk
        );
        if(!stat) {
            stat = {troubleKey, cardBalance, mastery, risk, ...this.#calculateDrawingProbability(troubleKey, cardBalance, mastery, risk)};
            this.#allCardDrawStats.push(stat);
        }
        return stat;
    }

    #calculateSuccessProbability(troubleKey, baseScore, mastery, risk){

        const possibleResults = this.getCombinations(troubleKey, mastery, risk);

        let nbCombinations = possibleResults.length * 1.0;
        let nbDisaster = 0;
        let nbEpicFail = 0;
        let nbFail = 0;
        let nbSuccess = 0;
        let nbCritical = 0;
        let nbExploit = 0;
        
        possibleResults.forEach( c => {

            if(c.skull >= 9) { nbDisaster++; } 
            else if(c.skull >= 6) { nbEpicFail++; } 
            else if(c.skull == 5 || c.success == 0 || c.success + baseScore < 0 ) { nbFail++; } 
            else if(c.success + baseScore < 3) { nbSuccess++; } 
            else if(c.success + baseScore < 6) { nbCritical++; } 
            else { nbExploit++; } 
        });

        const successChances = [
            successStat(nbDisaster, nbCombinations, 'disaster'),
            successStat(nbEpicFail, nbCombinations, 'epicFail'),
            successStat(nbFail, nbCombinations, 'failure'),
            successStat(nbSuccess, nbCombinations, 'success'),
            successStat(nbCritical, nbCombinations, 'critical'),
            successStat(nbExploit, nbCombinations, 'incredible'),
        ];
        return successChances;
    }


    #calculateDrawingProbability(troubleKey, cardBalance, mastery, risk) {

        const possibleResults = this.getCombinations(troubleKey, mastery, risk);
        let combinationsAmount = possibleResults.length * 1.0;
        let drawnByActorCases = [0, 0, 0, 0, 0, 0, 0];
        let drawnByOpponentCases = [0, 0, 0, 0, 0, 0, 0];
        
        possibleResults.forEach( c => {
            // If cardBalance > 0, it removes first faces allowing opponent to draw card
            // If cardBalance < 0, it removes first faces allowing the actor to draw card
            const cardDrawnByActor = cardBalance < 0 ? Math.clamp(c.card + cardBalance, 0, 7) : c.card;
            const cardDrawnByOpponent = cardBalance > 0 ? Math.clamp(c.opponentCard - cardBalance, 0, 7) : c.opponentCard;
            drawnByActorCases[cardDrawnByActor]++;
            drawnByOpponentCases[cardDrawnByOpponent]++;
        });

        const drawnByActorStats = [];
        for( let i = 0; i < drawnByActorCases.length; i++ ) {
            drawnByActorStats.push( 
                cardStat( drawnByActorCases[i], combinationsAmount, i, CARDS_COLOR_FOR_ACTOR[i], 'actor') 
            );
        }

        const drawnByOpponentStats = [];
        for( let i = 0; i < drawnByOpponentCases.length; i++ ) {
            drawnByOpponentStats.push( 
                cardStat( drawnByOpponentCases[i], combinationsAmount, i, CARDS_COLOR_FOR_OPPONENT[i], 'opponent') 
            );
        }
        return {
            opponentStats : drawnByOpponentStats,
            actorStats : drawnByActorStats,
        }
    }

    async displayStatMessage(validSuccessTokens, cardBalances, validMasteries, validRisks, {spiritTrouble="", selfTalk = false} = {}) {

        // Success lines
        const successLines = [];
        validSuccessTokens.forEach(baseScore => {
            const baseScoreLine = {baseScore, children: []};
            validMasteries.forEach(mastery => {
                const subtitle = translate( translationKey("AEDICES.stats.success.mastery", mastery), {DICE: mastery} );
                const masteryLine = {subtitle, mastery, children: []};
                validRisks.forEach(risk => {
                    const stats = this.getSucessStat(spiritTrouble, baseScore, mastery, risk );
                    const riskLine = {risk, stats};
                    masteryLine.children.push(riskLine);
                });
                baseScoreLine.children.push(masteryLine);
            });
            successLines.push(baseScoreLine);
        });

        // Card lines
        const cardLines = [];
        cardBalances.forEach(cardBalance => {
            validMasteries.forEach(mastery => {
                const subtitleKey = translationKey(
                    translationKey("AEDICES.stats.event", cardBalance, (_val)=> _val < 0, "cardsForOpponent", "cardsForActor"),
                    Math.abs(cardBalance)
                );
                const masteryPart = translate( translationKey("AEDICES.stats.success.mastery", mastery), {DICE: mastery} );
                const subtitle = masteryPart + " / " + translate(subtitleKey, {BALANCE: Math.abs(cardBalance)});

                const masteryLine = {subtitle, mastery, children: []};
                validRisks.forEach(risk => {
                    const element = this.getCardDrawStat(spiritTrouble, cardBalance, mastery, risk );
                    const actorStats = element?.actorStats ?? [];
                    const opponentStats = element?.opponentStats ?? [];
                    const riskLine = {risk, actorStats, opponentStats};
                    masteryLine.children.push(riskLine);
                });
                cardLines.push(masteryLine)
            });
        });

        const result = {
            trouble: parseSelectedTroubleKey(spiritTrouble),
            successLines,
            cardLines
        };
        result.displaySuccess = result.successLines.length > 0;
        result.displayCards = result.cardLines.length > 0;
        
        if( !result.displaySuccess && !result.displayCards ) { return; }
    
        const template = 'modules/ae-dices/resources/stats/stats-message.hbs';
        const html = await renderTemplate(template, result);
        const chatData = {
            user: game.user.id,
            content: html
        };
        if( selfTalk ) {
            chatData.whisper = [game.user.id];
        }
    
        await ChatMessage.create(chatData, {});
    
    }

    
    
    

    /*------------------------------------
     * GUI part
      ------------------------------------*/    

    /** @override */
    get title() {
        return 'Dice statistics';
    }

    /** @override */
    getData() {

        const withIcons = (tab) => {
            return tab.map( e => {
                return foundry.utils.mergeObject( {
                    icon: e.selected ? SELECT_ICON.selected : SELECT_ICON.notSelected
                }, e);
            });
        };

        const troubleIcon = (trouble, positive) => {
            const base = positive ? trouble.positive : trouble.negative;
            const key = trouble.key + "#" + base.key;
            return {
                key: key,
                name: base.name,
                icon: base.msgIcon,
                css: this.#selectedTrouble === key ? "selected" : "",
                style: `background-image: url('${base.msgIcon}')`,
                tooltip: base.name + "<br/>" + base.descriptionForTooltip
            }
        };

        const select = translate('AEDICES.stats.popup.button.selectAll');
        const unselect = translate('AEDICES.stats.popup.button.unselectAll');
        const troubles = game.aesystem.allAvailableTroubles();
        return {
            success: {
                css: "success",
                title: translate("AEDICES.stats.popup.success"),
                all: this.allSuccessValuesSelected ? unselect : select,
                values: withIcons(this.#successValues)
            },
            cardbalances: {
                css: "cardbalance",
                title: translate("AEDICES.stats.popup.cardbalance"),
                all: this.allCardBalanceTokensSelected ? unselect : select,
                values: withIcons(this.#cardBalanceValues)
            },
            masteries: {
                css: "mastery",
                title: translate("AEDICES.stats.popup.masteries"),
                all: this.allMasteryValuesSelected ? unselect : select,
                values: withIcons(this.#masteryValues)
            },
            risk: {
                css: "risk",
                title: translate("AEDICES.stats.popup.riskTaken"),
                all: this.allRiskValuesSelected ? unselect : select,
                values: withIcons(this.#riskValues)
            },
            troubles: {
                positive: troubles.map(t => troubleIcon(t, true)),
                negative: troubles.map(t => troubleIcon(t, false))
            }
        };
    }

    /** @override */
    activateListeners(html) {
        super.activateListeners(html);

        html.find(".success-values").on("click", ".clickable-box",  this.#onClickToggleSuccessBox.bind(this));
        html.find(".cardbalance-values").on("click", ".clickable-box",  this.#onClickToggleCardBalanceBox.bind(this));
        html.find(".mastery-values").on("click", ".clickable-box",  this.#onClickToggleMasteryBox.bind(this));
        html.find(".risk-values").on("click", ".clickable-box",  this.#onClickToggleRiskBox.bind(this));

        html.find(".all-line.success").on("click", ".all-button",  this.#onClickToggleAllSuccessBox.bind(this));
        html.find(".all-line.cardbalance").on("click", ".all-button",  this.#onClickToggleAllCardBalanceBox.bind(this));
        html.find(".all-line.mastery").on("click", ".all-button",  this.#onClickToggleAllMasteryBox.bind(this));
        html.find(".all-line.risk").on("click", ".all-button",  this.#onClickToggleRiskAllBox.bind(this));

        html.find(".final-line").on("click", ".check-button",  this.#onClickDisplayStats.bind(this));
        html.find(".value-section").on("click", ".trouble-line",  this.#onClickChooseTrouble.bind(this));
    }

    async #onClickToggleSuccessBox(event) {
        event.preventDefault();
        this._toggleOne( this.#successValues, event.currentTarget.dataset.score );
        this.render();
    }

    async #onClickToggleCardBalanceBox(event) {
        event.preventDefault();
        this._toggleOne( this.#cardBalanceValues, event.currentTarget.dataset.score );
        this.render();
    }

    async #onClickToggleMasteryBox(event) {
        event.preventDefault();
        this._toggleOne( this.#masteryValues, event.currentTarget.dataset.score );
        this.render();
    }

    async #onClickToggleRiskBox(event) {
        event.preventDefault();
        this._toggleOne( this.#riskValues, event.currentTarget.dataset.score );
        this.render();
    }

    async #onClickToggleAllSuccessBox(event) {
        event.preventDefault();
        this._toggleAll( this.#successValues, this.allSuccessValuesSelected );
        this.render();
    }

    async #onClickToggleAllCardBalanceBox(event) {
        event.preventDefault();
        this._toggleAll( this.#cardBalanceValues, this.allCardBalanceTokensSelected );
        this.render();
    }

    async #onClickToggleAllMasteryBox(event) {
        event.preventDefault();
        this._toggleAll( this.#masteryValues, this.allMasteryValuesSelected );
        this.render();
    }

    async #onClickToggleRiskAllBox(event) {
        event.preventDefault();
        this._toggleAll( this.#riskValues, this.allRiskValuesSelected );
        this.render();
    }

    async #onClickDisplayStats(event) {
        event.preventDefault();
        const selfTalk = parseInt(event.currentTarget.dataset.self);
        const validSuccessTokens = this.#successValues.filter(e => e.selected).map( e => e.value );
        const validCardBalances = this.#cardBalanceValues.filter(e => e.selected).map( e => e.value );
		const validMasteries = this.#masteryValues.filter(e => e.selected).map( e => e.value );
		const validRiskValues = this.#riskValues.filter(e => e.selected).map( e => e.value );

        await this.displayStatMessage(validSuccessTokens, validCardBalances, validMasteries, validRiskValues, {spiritTrouble: this.#selectedTrouble, selfTalk});
        this.close();
    }

    async #onClickChooseTrouble(event) {
        event.preventDefault();
        const div = event.currentTarget;
        const key = div.dataset.key;
        if( this.#selectedTrouble === key ) {
            this.#selectedTrouble = "";
        } else {
            this.#selectedTrouble = key;
        }
        this.render();
    }

    _toggleOne( values, score ) {
        const val = parseInt(score);
        const elem = values.find( v => v.value === val );
        if( elem ) {
            elem.selected = ! elem.selected;
        }
    }

    _toggleAll( values, allSelected ) {
        values.forEach( e => {
            e.selected = !allSelected;
        });
    }

}
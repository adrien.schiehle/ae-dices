export const AE_DICEPRESETS = {
	'success' : {
		type: "ds",
		shape: "d6",
		labels: [
			"modules/ae-dices/resources/dices/classic/d6-1.png",
			"modules/ae-dices/resources/dices/classic/d6-2.png",
			"modules/ae-dices/resources/dices/classic/d6-3.png",
			"modules/ae-dices/resources/dices/classic/d6-4.png",
			"modules/ae-dices/resources/dices/classic/d6-5.png",
			"modules/ae-dices/resources/dices/classic/d6-6.png"
		],
		icons: [
			"modules/ae-dices/resources/dices/classic/d6-1-icon.png",
			"modules/ae-dices/resources/dices/classic/d6-2-icon.png",
			"modules/ae-dices/resources/dices/classic/d6-3-icon.png",
			"modules/ae-dices/resources/dices/classic/d6-4-icon.png",
			"modules/ae-dices/resources/dices/classic/d6-5-icon.png",
			"modules/ae-dices/resources/dices/classic/d6-6-icon.png"
		],
		system: "AEDices"
	},
	'risk' : {
		type: "dr",
		shape: "d6",
		labels: [
			"modules/ae-dices/resources/dices/risk/d6-1.png",
			"modules/ae-dices/resources/dices/risk/d6-2.png",
			"modules/ae-dices/resources/dices/risk/d6-3.png",
			"modules/ae-dices/resources/dices/risk/d6-4.png",
			"modules/ae-dices/resources/dices/risk/d6-5.png",
			"modules/ae-dices/resources/dices/risk/d6-6.png"
		],
		icons: [
			"modules/ae-dices/resources/dices/risk/d6-1-icon.png",
			"modules/ae-dices/resources/dices/risk/d6-2-icon.png",
			"modules/ae-dices/resources/dices/risk/d6-3-icon.png",
			"modules/ae-dices/resources/dices/risk/d6-4-icon.png",
			"modules/ae-dices/resources/dices/risk/d6-5-icon.png",
			"modules/ae-dices/resources/dices/risk/d6-6-icon.png"
		],
		system: "AEDices"
	},
	'balance' : {
		type: "db",
		shape: "d2",
		labels: [
			"modules/ae-dices/resources/dices/balance/d2-1.png",
			"modules/ae-dices/resources/dices/balance/d2-2.png"
		],
		icons: [
			"modules/ae-dices/resources/dices/balance/d2-1-icon.png",
			"modules/ae-dices/resources/dices/balance/d2-2-icon.png"
		],
		system: "AEDices"
	},
	'complication' : {
		type: "dc",
		shape: "d2",
		labels: [
			"modules/ae-dices/resources/dices/complication/d2-1.png",
			"modules/ae-dices/resources/dices/complication/d2-2.png"
		],
		icons: [
			"modules/ae-dices/resources/dices/complication/d2-1-icon.png",
			"modules/ae-dices/resources/dices/complication/d2-2-icon.png"
		],
		system: "AEDices"
	}
};

export class SuccessDiceDef extends foundry.dice.terms.Die {
	
    constructor(termData) {
        super({
            number: termData.number,
            faces: 6,
            modifiers: termData.modifiers,
            options: termData.options,
			results: termData.results
        });
    }

    /* -------------------------------------------- */

    /** @override */
    static DENOMINATION = "s";

    /* -------------------------------------------- */

    /** @override */
    static getResultLabel(result) {
        return  '<img src="' + AE_DICEPRESETS.success.labels[result-1] + '" width="24" height="24" style="border:0px">';
    }
}

export class RiskDiceDef extends foundry.dice.terms.Die {
	
    constructor(termData) {
        super({
            number: termData.number,
            faces: 6,
            modifiers: termData.modifiers,
            options: termData.options,
			results: termData.results
        });
    }

    /* -------------------------------------------- */

    /** @override */
    static DENOMINATION = "r";

    /* -------------------------------------------- */

    /** @override */
    static getResultLabel(result) {
        return  '<img src="' + AE_DICEPRESETS.risk.labels[result-1] + '" width="24" height="24" style="border:0px">';
    }
}


export class BalanceTokenDef extends foundry.dice.terms.Die {
	
    constructor(termData) {
        super({
            number: termData.number,
            faces: 2,
            modifiers: termData.modifiers,
            options: termData.options,
			results: termData.results
        });
    }

    /* -------------------------------------------- */

    /** @override */
    static DENOMINATION = "b";

    /* -------------------------------------------- */

    /** @override */
    static getResultLabel(result) {
        return  '<img src="' + AE_DICEPRESETS.balance.labels[result-1] + '" width="24" height="24" style="border:0px">';
    }
}


export class ComplicationTokenDef extends foundry.dice.terms.Die {
	
    constructor(termData) {
        super({
            number: termData.number,
            faces: 2,
            modifiers: termData.modifiers,
            options: termData.options,
			results: termData.results
        });
    }

    /* -------------------------------------------- */

    /** @override */
    static DENOMINATION = "c";

    /* -------------------------------------------- */

    /** @override */
    static getResultLabel(result) {
        return  '<img src="' + AE_DICEPRESETS.complication.labels[result-1] + '" width="24" height="24" style="border:0px">';
    }
}


import { AE_DICEPRESETS } from './DiceDefinitions.js';
import { generateUUID } from '../tools/uuid.js';

export const CLASSICDICE_FACES = [
    {success:0, skull:2, power:0, card:0, opponentCard:0, depends:0},
    {success:0, skull:0, power:1, card:0, opponentCard:0, depends:0},
    {success:0, skull:0, power:0, card:1, opponentCard:0, depends:0},
    {success:1, skull:0, power:0, card:0, opponentCard:0, depends:0},
    {success:1, skull:0, power:0, card:0, opponentCard:0, depends:0},
    {success:1, skull:0, power:0, card:0, opponentCard:0, depends:1}
];

export const RISKDICE_FACES = [
    {success:0, skull:3, power:0, card:0, opponentCard:0, depends:0},
    {success:0, skull:1, power:1, card:0, opponentCard:0, depends:0},
    {success:0, skull:0, power:0, card:0, opponentCard:1, depends:0},
    {success:0, skull:1, power:0, card:0, opponentCard:0, depends:1},
    {success:1, skull:0, power:0, card:0, opponentCard:0, depends:1},
    {success:2, skull:0, power:0, card:0, opponentCard:0, depends:0}
];


const DMG_NOTARGET_ICON = 'modules/ae-dices/resources/noTarget.png';
const HEAL_ICON = 'modules/ae-dices/resources/dices/healIcon.webp';

/**
 * Generates and id for this spell and add it to context
 * @param {object} result The whole dice result
 * @param {object} spellData spell data that should be add to context
 */
const addSpellToContext = (result, spellData) => {
	const data = foundry.utils.mergeObject( { }, spellData );
	data.id = generateUUID();

	let level = data.spell?.level ?? 0;
	if( spellData.givesActiveEffect ) {
		// Spells giving buffs or debuffs have their level affected by success type
		if( result.action.success ) {
			level = Math.max( 0, level - 1 );
		} else if ( result.action.incredible ) {
			level = Math.min( 6, level + 1 );
		}
	}
	data.effectiveLevel = level;

	result.context.spellData = data;
}

/**
 * If you roll 3 or more skulls, it will be an eipcFail or worse
 * If you don't roll any success, it will be a failure.
 * Success (or better) can be attained if you rolled success faces sum above advantage difficulty
 */
const deduceActionState = (result) => {

	const step = 0 - result.scoreBase;
	const successVal = result.roll.success_value;

	if( result.roll.failure_value >= 9 ) {
		result.action.disaster = true;

	} else if( result.roll.failure_value >= 6 ) {
		result.action.epicFail = true;

	} else if( result.roll.failure_value == 5 ) {
		result.action.failure = true;
		result.action.failureSource = "complication";

	} else if( successVal == 0 ) {
		result.action.failure = true;
		result.action.failureSource = "noSuccess";
		
	} else if( successVal < step ) {
		result.action.failure = true;
		result.action.failureSource = "notEnough";
		
	} else if( successVal >= step + 6 ) {
		result.action.incredible = true;
		result.action.successLevel = 2;
		
	} else if( successVal >= step + 3 ) {
		result.action.critical = true;
		result.action.successLevel = 1;
		
	} else {
		result.action.success = true;
		result.action.successLevel = 0;
	}

	result.action.successOrBetter = result.action.successLevel >= 0;
}

/** 
 * Each card face on risk dice gives a card to the opponent/
 * For card face on classic dice, the person in control retrieve the cards.
 */
const prepareCardsData = (result) => {

	// Choose who wil receive the cards
	const mainActor = result.actor;
	
	// First: Give cards to opponent
	if( result.roll.cards_for_opponent ) {
		const userToDrawCards = game.aesystem.actorTools.chooseUserForCardDraw(mainActor, true);
		if( userToDrawCards ) {
			result.cardsToDraw.push({
				user_id: userToDrawCards.id,
				nb: result.roll.cards_for_opponent
			});
		}
	}

	// Second : Give cards to main actor
	if( result.roll.cards_for_actor ) {
		const userToDrawCards = game.aesystem.actorTools.chooseUserForCardDraw(mainActor, false);
		if( userToDrawCards ) {
			result.cardsToDraw.push({
				user_id: userToDrawCards.id,
				nb: result.roll.cards_for_actor
			});
		}
	}
}

/**
 * Discard all advantage tokens
 * Gain 1 token if the action resuts in a simple failure.
 */
 const updateAdvantageTokens = (result) => {
	let advantage = 0;
	if( result.action.failure) {
		advantage += 1;
	}
	return advantage;
}

/**
 * Power tokens represents the level of power the character can unleash.
 * Maximul level is 3.  (the number of effect dices)
 * After dice are rolled, each power face grant a power token to the actor.
 * Some maneuvers refills the power token, it will be given back here.
 */
const updatePowerTokens = (result) => {
	const actor = result.actor;
	const powerTokens = actor.power + result.roll.power_value + result.action.additionalPowerGain;

	if( powerTokens != actor.power ) {
		console.log( 'AE-Dices | Updating power tokens : ' + actor.power + ' -> ' + powerTokens );
	}

	return powerTokens;
}

/** Prepare communion data part. Spirit cards will be drawn after displaying result. Not done here */
const prepareCommunionData = (result) => {

	const divine = result.context?.divine;
	if( !divine ) { return; } // No communion data

	// On simple failure : Do nothing
	if( result.action.failure ) { return; }

	// Otherwise : Spirits are consumed
	result.context.communion = {};
	const communion = result.context.communion;
	communion.consumedSpirits = [...divine.usedSpirits];

	// On disasters, all spirits are going away
	if( result.action.disaster ) {
		divine.revokeAllSpirits = true;
		communion.consumedSpirits = result.actor.cards.spirits.map(s => s.card.id);

	} else if( result.action.successOrBetter ) {
		addSpellToContext(result, divine.spell);
		if( !result.actor.isNPC ) { // Drain are manually handled for NPC, since they don't really have endurance
			divine.drain.apply = true;
			result.action.healthChange.fatigue += divine.drain.fatigue;
			result.action.healthChange.damage += divine.drain.damage;
		}
	}
}

/** Arcane circle will be updated on actor after displaying result. */
const prepareArcaneCircleData = (result) => {

	const arcane = result.context?.arcane;
	if( !arcane ) { return; } // No arcane data
	
	arcane.deleteCircle = ! result.action.failure; // All the other cases let to the circle deletion
	if( result.action.disaster ) {
		arcane.circleExplosion = true;
	}

	if(result.action.successOrBetter) {
		addSpellToContext(result, arcane.spell);
		if( !result.actor.isNPC ) { // Drain are manually handled for NPC, since they don't really have endurance
			arcane.drain.apply = true;
			result.action.healthChange.fatigue += arcane.drain.fatigue;
			result.action.healthChange.damage += arcane.drain.damage;
		}
	}
}

/** Arcane circle will be updated on actor after displaying result. */
const prepareMelodyData = (result) => {

	const melody = result.context?.melody;
	if( !melody ) { return; } // No melody data

	if( result.action.successOrBetter ) {
		melody.upgrade = true;
		
		// NPC don't have set values for melodies. Should be manually handled
		const melodyKnowledge = result.actor.isNPC ? 99 : result.actor.songs.getMelody(melody.melodyKey).level;
		const aboveKnowledge = melody.melodyLevel > melodyKnowledge;
	
		if( result.action.incredible ) {
			if( melody.dissonance != 0 ) {
				melody.melodyFortified = true;
				melody.dissonance = 0;
			}
		} else if( result.action.success || aboveKnowledge ) {
			// Triggered on partial success or if the desired level is above the character knowledge
			melody.dissonanceAdded = true;
			melody.dissonance ++;
		}

		// Add a line which works as a spell for timetracker and message display
		addSpellToContext(result, melody.melodyAsSpell);

	} else if( result.action.disaster || result.action.epicFail ) {
		melody.stop = true;
	}
}

/** If a weapon has been described. Consider it will inflict damage in case the attack is a success */
const prepareDamageInfo = (result) => {

	result.context.damages = [];

	if( !result.action.successOrBetter || !result.context ) { return; }

	const findDamageSource = (res) => {

		const context = res.context;
		const actor = res.actor;
		const weapon = context.actor?.weapon;
		if( weapon ) { 
			return {
                label: weapon.item.name,
                icon: weapon.item.img,
                damage: weapon.damage,
				damageTypes: [weapon.element.key],
				damageTics: 1,
				weaponAttack:{
					tokenId: actor.tokenId
				}
            };
		}
		
		const spellData = context.spellData;
		if( spellData && spellData.dealsDamage ) { 
			return {
				label: spellData.spell.name,
				icon: spellData.damageTypes[0].icon,
				damage: spellData.damage,
				damageTypes: spellData.damageTypes.map(dt => dt.key),
				damageTics: spellData.damageTics,
				spellAttack: {
					fullDamageTypes: spellData.damageTypes
				}
			}; 
		}
		return null;
	}

	const dmgSource = findDamageSource( result );
	if( !dmgSource ) { return; }

	// Damage Line (one by success level)
	const damageLine = dmgSource.damage[result.action.successLevel];

	// msgSource will be copied in each message line an altered through alterLineDependingOnTarget
	const msgSource = foundry.utils.mergeObject({}, dmgSource);
	msgSource.spellAttack = null; // Some of dmgSource should not be transfered inside each line
	msgSource.repeatLines = dmgSource.damageTics;

	const calculateDamageAmount = (line, target) => {
		
		if( !target ) { // Specific update for defaultLine
			line.amount = damageLine[0].value;
			line.damageLine = damageLine;
			return ; 
		}
		
		const possibleElementDamages = dmgSource.damageTypes.map( dt => {
			const protectionLevel = target.equipment.currentArmorProtection.find( p => p.element === dt )?.value ?? 0;;
			const dmg = damageLine.find( d => d.armorLevel === protectionLevel )?.value ?? 0;
			return {
				element: dt,
				damageValue: dmg
			};
		});
		possibleElementDamages.sort( (a,b) => b.damageValue - a.damageValue );
		const chosen = possibleElementDamages[0];

		line.amount = chosen.damageValue;
		if( dmgSource.spellAttack ) {
			line.element = chosen.element;
			line.icon = dmgSource.spellAttack.fullDamageTypes.find( dt => dt.key === chosen.element ).icon;
		}
	}

	const options = {
		alterLine: calculateDamageAmount
	}
	result.context.damages = reduceTargetsIntoMessageLines([], msgSource, options);
}

	

/** Some spells can grant heals to their targets. */
const prepareHealInfo = (result) => {

	result.context.heals = [];

	if( !result.action.successOrBetter || !result.context ) { return; }

	// Heal source
	const spellData = result.context?.spellData;
	if( !spellData || !spellData.healsDamage ) { return; }

	const healData = spellData.heal;
	healData.level = spellData.effectiveLevel;
	healData.healAmount = 2 + Math.ceil(spellData.effectiveLevel / 2.0) * 2;
	healData.fatigueRatio = 1 + Math.floor(spellData.effectiveLevel / 2.0)

	const msgSource = {
		label: spellData.spell?.name ?? spellData.effect?.name,
		icon: spellData.spell?.icon ?? HEAL_ICON,
		// amount: healData.healAmount, Will be set inside calculateHealAmount
		fatigueRatio: healData.fatigueRatio,
		fromResistances: healData.fromResistances,
		repeatLines: 1
	};

	const calculateHealAmount = (line, target) => {
		if( !target ) { // Specific update for defaultLine
			line.amount = healData.healAmount;
			return ; 
		}
		const needed = target.maxHealth - target.health;
		line.amount = Math.min(healData.healAmount, needed);
	}

	const options = {
		alterLine: calculateHealAmount
	}

	// Build heal array by merging targets info and msgSource
	result.context.heals = reduceTargetsIntoMessageLines([], msgSource, options);
}
	

/** Some spells can grant active effects to their targets */
const prepareActiveEffectInfo = (result) => {

	result.context.activeEffects = [];

	if( !result.action.successOrBetter || !result.context ) { return; }

	// Active Effect Data
	const spellData = result.context?.spellData;
	if( !spellData || !spellData.givesActiveEffect ) { return; }

	const effectData = spellData.activeEffectData;
	effectData.id = spellData.id;
	effectData.level = spellData.effectiveLevel;

	const msgSource = {
		label: effectData.label,
		icon: effectData.icon,
		amount: spellData.effectiveLevel,
		repeatLines: 1
	};

	const addDataForDefaultLine = (line, target) => {
		if( !target ) {
			line.activeEffectData = effectData;
			line.restrictTargetSelection = spellData.targetsOnlyCaster;
		}
	}

	const options = {
		alterLine: addDataForDefaultLine
	}

	// Build active effect array by merging targets info and msgSource
	result.context.allTargets = spellData.targetsOnlyCaster ? [result.actor.tokenId] : [];
	result.context.activeEffects = reduceTargetsIntoMessageLines(result.context.allTargets, msgSource, options);
}

/** Some action adds more informative lines on success */
const prepareAdditionalDataOnSuccess = (result) => {

	// Convenience functions
	//------------------------
	const retrieveOnSuccessModifiers = (context) => {

		const weaponId = context.actor?.weaponId;
		const weapon = result.actor.equipment.weapons.find( w => w.item.id === weaponId ) ?? null;
		if( weapon ) { 
			return {
				source: {
					icon: weapon.item.img,
					name: weapon.item.name
				},
				modifiers: weapon.onAttackSuccess
			}
		}
		return null;
	}

	// Some verifications
	//------------------------
	result.context.moreData = [];
	if( !result.action.successOrBetter || !result.context ) { return; }

	// Setting result.context.moreData
	//------------------------
	let moreData = [].concat( );
	moreData = moreData.concat( result.context.onSuccess );
	
	const onSuccess = retrieveOnSuccessModifiers(result.context);
	if( onSuccess ) {
		const modifier = onSuccess.modifiers[result.action.successLevel];
		if( modifier.selfDamage ) {
			const msg = game.i18n.localize('AEDICES.chat.rollDetail.onAttackSuccess.selfDamage')
							.replace('SOURCE', onSuccess.source.name)
							.replace( 'AMOUNT', modifier.selfDamage );
			moreData.push( {
				icon: onSuccess.source.icon,
				name: msg
			});
			result.action.healthChange.damage += modifier.selfDamage;
		}
		if( modifier.powerGain ) {
			const msg = game.i18n.localize('AEDICES.chat.rollDetail.onAttackSuccess.powerGain')
							.replace('SOURCE', onSuccess.source.name)
							.replace( 'AMOUNT', modifier.powerGain );
			moreData.push( {
				icon: onSuccess.source.icon,
				name: msg
			});
			result.action.additionalPowerGain += modifier.powerGain;
		}
	}

	// Spells can also add more data
	//------------------------
	const spellMsgs = result.context?.spellData?.additionalMessages ?? [];
	moreData.push(...spellMsgs);

	result.context.moreData = moreData;
}

/**
 * Used by prepareDamageInfo, prepareHealInfo, prepareActiveEffectInfo to build an array of messages
 * that can be later used when rollMessage is displayed and during TargetPopup selection
 * @param {object[]} allTargets array of all targets token ids
 * @param {object} source Message base
 * @param {function (object, actor)} alterLine If present, modify msgLine by adding some data depending on actors
 * @returns Mesages lines usable by DiceMessage
 */
const reduceTargetsIntoMessageLines = ( allTargets, source, {alterLine = null} = {} ) => {
	const tools = game.aesystem.actorTools;
	const result = allTargets.reduce( (myArray, tokenId) => {

		const tokenActor = tools.getActorOrTokenActor({
            tokenId: tokenId, 
            allowCurrentSelection: false
        });
		if( tokenActor) {
			const data = {
				target: {
					tokenId: tokenActor.tokenId,
					id: tokenActor.id,
					name: tokenActor.name,
					icon: tokenActor.img
				}
			};
			foundry.utils.mergeObject(data, source);
			if( alterLine ) { // Damage is replicated by the amount of tics
				alterLine(data, tokenActor);
			}

			[...Array(source.repeatLines).keys()].forEach( _e => {
				myArray.push( data );
			});

		}

		return myArray;

	}, []);


	// Add default heal when target wasn't selected
	const defaultData = {
		target: {
			tokenId: null,
			id: null,
			name: '',
			icon: DMG_NOTARGET_ICON
		}
	}
	foundry.utils.mergeObject( defaultData, source );
	if( alterLine ) { 
		alterLine(defaultData, null);
	}

	[...Array(source.repeatLines).keys()].forEach( _e => {
		result.push( defaultData );
	});
	return result;
}

/**
 * Parse result and update character token by spend tokens for each token faces.
 * Return Modifier result
 */
const consolidateResults = async (result) => {

	deduceActionState(result);
	prepareCardsData(result);
	prepareArcaneCircleData(result);
	prepareCommunionData(result);
	prepareMelodyData(result);
	prepareDamageInfo(result);
	prepareHealInfo(result);
	prepareActiveEffectInfo(result);
	prepareAdditionalDataOnSuccess(result);

	const advantage = updateAdvantageTokens(result);
	const mastery = 0;
	const power = updatePowerTokens(result);
	await result.actor.setAllBasicTokens( advantage, mastery, power, 0 );
}


const interpretComplications = (result, complicationAmount) => {

	const negative = complicationAmount < 0;

	for( let i = 0; i < Math.abs(complicationAmount); i++ ) {
		result.roll.failure_icons.push(AE_DICEPRESETS.complication.icons[negative ? 1 : 0]);
		result.roll.failure_value += negative ? -1 : 1;
	}
}

const interpretSuccessDice = (result, diceFace) => {
	interpretDice(result, AE_DICEPRESETS.success.icons[diceFace-1], CLASSICDICE_FACES[diceFace-1]);
}

const interpretRiskDice = (result, diceFace) => {
	interpretDice(result, AE_DICEPRESETS.risk.icons[diceFace-1], RISKDICE_FACES[diceFace-1]);
}

const interpretDice = (result, icon, faceValue) => {
	result.roll.all_icons.push(icon);

	if( faceValue.skull ) {
		result.roll.failure_icons.push(icon);
		result.roll.failure_value += faceValue.skull;
	} 
	if( faceValue.card ) {
		result.roll.card_icons.push(icon);
		let amount = faceValue.card;
		// First : we discard tokens
		if( result.roll.card_balance < 0 ) {
			let diff = Math.min(0 - result.roll.card_balance, amount);
			result.roll.card_balance += diff;
			amount -= diff;
		}
		result.roll.cards_for_actor += amount;
	} 
	if( faceValue.opponentCard ) {
		result.roll.card_icons.push(icon);
		let amount = faceValue.opponentCard;
		// First : we discard tokens
		if( result.roll.card_balance > 0 ) {
			let diff = Math.min(result.roll.card_balance, amount);
			result.roll.card_balance -= diff;
			amount -= diff;
		}
		result.roll.cards_for_opponent += amount;
	} 
	if( faceValue.power ) {
		result.roll.power_icons.push(icon);
		result.roll.power_value += faceValue.power;
	} 
	if( faceValue.success ) {
		result.roll.success_icons.push(icon);
		result.roll.success_value += faceValue.success;
	} 
	if( faceValue.depends ) {
		result.actor.cards.troubles.forEach( trouble => {
			handleGuardianSpiritTroubles(result, trouble);
		});
	} 
}

/**
 * Will modifiy dice values if the guardian spirit cause troubles
 * @param {object} result 
 * @param {int[]} sucessDiceFace 
 * @param {int[]} riskDiceFace 
 */
const handleGuardianSpiritTroubles = (result, trouble) => {

	const effectBase = trouble.isPositive ? trouble.effect.positive : trouble.effect.negative;
	const icon = effectBase.msgIcon;

	if( effectBase.skulls ) {
		result.roll.failure_icons.push(icon);
		result.roll.failure_value += effectBase.skulls;
	} 
	if( effectBase.events ) {
		result.roll.card_icons.push(icon);
		if( effectBase.events ) {
			result.roll.cards_for_actor += effectBase.events;
		} else {
			result.roll.cards_for_opponent += 0 - effectBase.events;
		}
	} 
	if( effectBase.powers ) {
		result.roll.power_icons.push(icon);
		result.roll.power_value += effectBase.powers;
	} 
	if( effectBase.success ) {
		result.roll.success_icons.push(icon);
		result.roll.success_value += effectBase.success;
	} 
	if( effectBase.health ) {
		result.roll.power_icons.push(icon); // Will be displayed inside power icons
		result.action.healthChange.damage -= effectBase.health;
	} 
	if( effectBase.initiative ) {
		result.roll.power_icons.push(icon); // Will be displayed inside power icons
		result.action.duration += effectBase.initiative;
	}
}


export class AEDiceInterpretor {

	static async interpretRoll(actor, rollData) {

		const result = {
			actor : actor,
			context: rollData.context,
			scoreBase: rollData.scoreBase,
			roll: {
				all_icons : [],
				success_icons : [],
				success_value : 0,
				failure_icons : [],
				failure_value : 0,
				power_icons : [],
				power_value : 0,
				card_icons : [],
				card_balance : rollData.context.roll.cardBalance,
				cards_for_actor: 0,
				cards_for_opponent: 0,
				risk: rollData.risk
			},
			action: {
				disaster: false,
				epicFail: false,
				failure: false,
				success: false,
				critical: false,
				incredible: false,
				successOrBetter: false,
				successLevel: -1,
				healthChange: {
					damage: 0,
					fatigue: 0
				},
				additionalPowerGain: 0,
				duration: 0
			},
			cardsToDraw: []
		};

		rollData.successResult.forEach( (item) => { interpretSuccessDice(result, item); });
		rollData.riskResult.forEach( (item) => { interpretRiskDice(result, item); });
		interpretComplications(result, rollData.context.roll.complication );
		await consolidateResults(result);
		
		return result;
	}

	
}
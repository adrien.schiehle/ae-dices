import {SuccessDiceDef, RiskDiceDef} from './DiceDefinitions.js';
import { AE_DICEPRESETS } from './DiceDefinitions.js';

const AE_DICESYSTEM = {
	id: "AEDices", 
	name: "Acaria Empire All Dices"
};

const DICE_DEFAULT_CONFIG = { 
	colorset: 'custom',
	texture: 'wood',
	material: 'wood',
	font: 'auto',
	labelColor: '#000000',
	labelColorSelector: '#000000',
	diceColor: '#ffffff',
	diceColorSelector: '#ffffff',
	outlineColor: '#000000',
	outlineColorSelector: '#000000',
	edgeColor: '#ffffff',
	edgeColorSelector: '#ffffff'
};
const DICE_DEFAULT_APPEARENCE = {
	labelColor: '#000000',
	diceColor: '#ffffff',
	outlineColor: '#000000',
	edgeColor: '#ffffff',
	texture: "none",
	material: "auto",
	font: "auto",
	colorset: "custom",
	system: "standard"
};

export class DiceLoader {

	constructor(dice3d) {

		this.dice3d = dice3d;
		this._loadResources();
	}
	
	_loadResources() {
		
		this.dice3d.addSystem(AE_DICESYSTEM, 'preferred');
		
		const flag = game.user.getFlag("dice-so-nice", "appearance") ?? DICE_DEFAULT_APPEARENCE;
		if( flag.colorset == 'white' ) delete flag.colorset; // FIXE Was previously badly initialized. Should be removed later when everyone has fixed his settings

		Object.values(AE_DICEPRESETS).forEach( preset => {
			this.dice3d.addDicePreset(preset, preset.shape);
			flag[preset.type] = DICE_DEFAULT_CONFIG;
		});
		game.user.setFlag("dice-so-nice", "appearance", flag);
	}
	
	/**
	 * 
	 * @param {Roll} roll dice rolled by foundry
	 * @param {object} difficulty Success step and difficulty
	 * @param {int[]} replacedDiceFace If some dice where already rolled elsewhere (with fixed values)
	 * @returns rollData
	 */
	retrieveRollDataFromMessage(roll, difficulty, replacedDiceFace) {
		
		let rollData = {
			successResult : [],
			riskResult : [], 
			scoreBase: difficulty.scoreBase ?? 0,
			risk: difficulty.risk ?? 0,
		};

		// Add replacedDiceFace
		if( replacedDiceFace.length > 0 ) {
			rollData.successResult.push(...replacedDiceFace);
		}
		
		// Build result data
		let diceResults = roll.terms;
		for( let i = 0; i < diceResults.length; i++ ) {
			
			if( diceResults[i] instanceof SuccessDiceDef ) {
				
				rollData.successIndex = i;
				
				let results = diceResults[i].results;
				for( let j = 0; j < results.length; j++ ) {
					rollData.successResult.push( results[j].result );
				}
				
				
			} else if( diceResults[i] instanceof RiskDiceDef ) {
				
				rollData.riskIndex = i;
				
				let results = diceResults[i].results;
				for( let j = 0; j < results.length; j++ ) {
					rollData.riskResult.push( results[j].result );
				}
			}
		}

		return rollData;
	}

}
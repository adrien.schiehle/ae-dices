
const successPrefix = (result) => {

    if( result.action.disaster ) {
        return 'disaster';
    } else if( result.action.epicFail ) {
        return 'epicFail';
    } else if( result.action.failure ) {
        return 'failure';
    } else if( result.action.success ) {
        return 'success';
    } else if( result.action.critical ) {
        return 'critical';
    } else {
        return 'incredible';
    }
}

/*
* For coloring success label
*/
const deduceSuccessClass = (result) => {
    let cssClass = '';
    if( result.action.disaster || result.action.epicFail ) {
        cssClass = 'bigfailure';
    } else if( result.action.failure ) {
        cssClass = 'failure';
    } else if( result.action.success ) {
        cssClass = 'success';
    } else if( result.action.critical ) {
        cssClass = 'bigsuccess';
    } else {
        cssClass = 'incredible';
    }
    return cssClass;
}


/**
 * It's a roll despite how it looks.
 * Apply same rules that for classic rolls
 */
 const applyCurrentRollModeInSetting = (data) => {

    const rollMode = game.settings.get("core", "rollMode");
    if ( ["gmroll", "blindroll"].includes(rollMode) ) data.whisper = ChatMessage.getWhisperRecipients("GM");
    if ( rollMode === "blindroll" ) data.blind = true;
    if ( rollMode === "selfroll" ) data.whisper = [game.user.id];
}

/**
 * Display the HTML message.
 * Once it has been built
 */
const sendMessage = async(html, actor, result) => {

    const allTargets = result.context?.allTargets ?? [];
    const damageDealt = result.context?.damages ?? [];
    const healDone = result.context?.heals ?? [];
    const activeEffects = result.context?.activeEffects ?? [];
    
    var chatData = {
        flags: {
            acariaempire: {
                aeroll: {
                    targets: allTargets,
                    damageDealt: damageDealt,
                    healDone: healDone,
                    activeEffects: activeEffects
                }
            }
        },
        content: html
    };
    applyCurrentRollModeInSetting(chatData);
    return actor.sendMessage(chatData);
}

const enrichRollResult = (result) => {
    const roll = foundry.utils.duplicate(result.roll);

    // Difficulty infos
    const base = 0 - result.scoreBase;
    const steps = [
        Math.max(base, 1),
        Math.max(base+3, 1),
        Math.max(base+6, 1),
    ]
    roll.successSteps = '' + steps[0] + ' / ' + steps[1] + ' / ' + steps[2];

    // Success details
    roll.successLabel = game.i18n.localize('AEDICES.chat.result.' + successPrefix(result) );
    roll.successClass = deduceSuccessClass(result);
    roll.displaySuccessHint = result.action.failure;
    roll.successHint = {
        displayed: result.action.failure,
        label: game.i18n.localize('AEDICES.chat.rollDetail.success.hint.' + result.action.failureSource)
    }
    
    roll.summaryIcons = roll.all_icons;

    // Power
    roll.displayPower = roll.power_icons.length > 0;
    roll.powerDesc = game.i18n.localize('AEDICES.chat.rollDetail.power.granted').replace('NB', roll.power_value);

    return roll;
}

export class AEDiceMessage {
	
	async displayRollResult(result, actor) {

	    let data = {
	        context: this.#buildContextForRollMessage(result),
            roll: enrichRollResult(result),
            circle: this.#buildCirclePart(result),
            communion: this.#buildCommunionPart(result),
            melody: this.#buildMelodyPart(result),
            targets: this.#buildTargetsPart(result),
            moreData: this.#addOnSuccessLinesIfNeeded(result),
            spell: this.#buildSpellDescription(result)
	    };

	    let html = await renderTemplate('modules/ae-dices/resources/dices/rollMessage.hbs', data);
	    return sendMessage(html, actor, result);
	}

    #buildSpellDescription(result) {
        
        const data = [];
        const spellData = result.context?.spellData;

        if( spellData ) {
            // Spell
            const spell = spellData.spell;
            if( spell ) {

                const dataset =[
                    {key: 'action', value: 'wa-frame'},
                    {key: 'shortcut', value: spell.shortcut},
                    {key: 'child', value: spell.shortcutChild},
                ];

                data.push(
                    {icon: spell.icon, title: spell.name, overlay: spell.levelIcon, dataset: dataset }
                );
            }

            // Shape
            const shape = spellData.shape;
            if( shape ) {
                const caracValue = (carac) => {
                    const val = shape[carac];
                    if( val || val == 0 ) return val;
                    return null;
                }
    
                const shapeDataset = [
                    {key: 'action', value: 'shape-display'},
                    {key: 'shape', value: shape.key},
                    {key: 'clone', value: caracValue('clone')},
                    {key: 'range', value: caracValue('range')},
                    {key: 'areaOfEffect', value: caracValue('areaOfEffect')},
                    {key: 'duration', value: caracValue('duration')}
                ];
                if( spell ) {
                    shapeDataset.push(
                        { key: 'spell', value: spell.key },
                        { key: 'spell-level', value: spell.level }
                    );
                }
    
                data.push(
                    {icon: shape.icon, title: shape.fullDesc.title, dataset: shapeDataset }
                );
            }
            
            // Add tooltip management for each line
            data.forEach( line => {
                // Clickabe ?
                const action = line.dataset?.find( d => d.key === 'action' )?.value;
                line.dataset = (line.dataset ?? []).filter( d => d.key != 'action');
                line.css = action ? 'clickable '+ action : '';

                // Overlay
                line.hasOverlay = line.overlay ? true: false;

                // Line title can sometime be really big
                if( line.title.length > 50 ) {
                    line.css += ' reduced-title';
                }
            });
        }
        return data;
    }

    #buildCirclePart(result) {

        const spellIcon = 'systems/acariaempire/resources/interaction/circle/token_back.png';
        const arcane = result.context?.arcane;
    
        const lines = [];
        const showCircle = arcane?.deleteCircle && !result.action.successOrBetter; 
        if( showCircle ) {
            // Description in case circle has been modified
            let descKey = 'AEDICES.chat.circle.';
            if( arcane.circleExplosion ) { descKey += 'explosion'; }
            else { descKey += 'delete'; }
            
            lines.push(
                {icon: spellIcon, title: game.i18n.localize(descKey) }
            );
        }

        lines.push(...this.#addDrainLines(result, arcane));

        return lines;
    }

    #buildCommunionPart(result) {

        const portrait = result.context.actor.portrait;
        const divine = result.context?.divine;

        const lines = [];
        if(divine?.revokeAllSpirits) {
            lines.push({
                icon: portrait, 
                title: game.i18n.localize('AEDICES.chat.communion.revokeAllSpirits')
            });
        }
        lines.push(...this.#addDrainLines(result, divine));
        
        return lines;
    }

    #addDrainLines(result, base) {
        const portrait = result.context.actor.portrait;
        const lines = [];
        if(base?.drain.apply) {
            if( base.drain.fatigue > 0 ) {
                lines.push({
                    icon: portrait, 
                    title: game.i18n.localize('AEDICES.chat.drain.fatigue').replace('DRAIN', base.drain.fatigue)
                }); 
            }
            if( base.drain.damage > 0 ) {
                lines.push({
                    icon: portrait, 
                    title: game.i18n.localize('AEDICES.chat.drain.damage').replace('DRAIN', base.drain.damage)
                }); 
            }
        }
        return lines;
    }

    #buildMelodyPart(result) {

        const portrait = result.context.actor.portrait;
        const dissonanceIcon = 'systems/acariaempire/resources/interaction/melody_dissonance.png';
        const data = [];

        const melodyCtx = result.context?.melody;
        if( melodyCtx?.stop ) {
            data.push( {
                icon: portrait,
                desc: game.i18n.localize('AEDICES.chat.melody.stop')
            });
        }
        if( melodyCtx?.dissonanceAdded ) {
            data.push( {
                icon: dissonanceIcon,
                desc: game.i18n.localize('AEDICES.chat.melody.dissonanceAdded')
            });
        }
        if( melodyCtx?.melodyFortified ) {
            data.push( {
                icon: dissonanceIcon,
                desc: game.i18n.localize('AEDICES.chat.melody.melodyFortified')
            });
        }
        return data;
    }

    #buildTargetsPart(result) {

        const data = [];

        // 1 - Damages
        //--------------------
        const allDamages = result.context?.damages ?? [];
        // Display default damage only if there is no targets
        const dmgWithTargets = allDamages.filter( dmg => dmg.target.tokenId );
        const damages = dmgWithTargets.length ? dmgWithTargets : allDamages;
        damages.forEach( dmg => {
            const desc = game.i18n.localize('AEDICES.chat.rollDetail.damage.' + successPrefix(result))
                            .replace('SOURCE', dmg.label);

            data.push({
                desc: desc,
                target: dmg.target,
                detail: dmg,
                type: 'damage'
            });
        });

        // 2 - Heals
        //--------------------
        const allHeals = result.context?.heals ?? [];
        const heals = allHeals.length == 1 ? allHeals : allHeals.filter( heal => heal.target.tokenId );
        heals.forEach( heal => {

            const ratio = heal.fatigueRatio;
            const fatigue = Math.ceil( 1.0 * heal.amount / ratio );
            const suffix = heal.amount > 1 ? 'multiple' : 'single';
            const desc = game.i18n.localize('AEDICES.chat.rollDetail.heal.' + suffix)
                            .replace('SOURCE', heal.label)
                            .replace('HEAL', heal.amount)
                            .replace('FATIGUE', fatigue);

            data.push({
                desc: desc,
                target: heal.target,
                detail: heal,
                type: 'heal'
            });
        });

        // 3 - Active effects
        //--------------------
        const allActiveEffects = result.context?.activeEffects ?? [];
        const activeEffects = allActiveEffects.length == 1 ? allActiveEffects : allActiveEffects.filter( heal => heal.target.tokenId );
        activeEffects.forEach( activeEffect => {

            const desc = game.i18n.localize('AEDICES.chat.rollDetail.activeEffect')
                            .replace('SOURCE', activeEffect.label);

            data.push({
                desc: desc,
                target: activeEffect.target,
                detail: activeEffect,
                type: 'activeEffect'
            });
        });
        

        return data;
    }

    #addOnSuccessLinesIfNeeded(result){
        const lines = result.context?.moreData ?? [];
        return lines.map( line => {
            const data = {
                css: line.shortcut ? 'wa-frame' : '',
                hasIcon : line.icon ? true : false,
                hasOverlay : line.overlay ? true : false
            };
            if( data.hasOverlay ) { data.css += ' with-overlay'; }
            return foundry.utils.mergeObject(data, line);
        });
    }
	
    /*
     * Fill additinal data insidie roll context.
     */
	#buildContextForRollMessage(result) {
        const context = foundry.utils.duplicate( result.context );

        context.actor.actionFlavorDisplayed = context.actor.actionFlavor != '';

        // Roll resume
        context.roll.title = game.i18n.localize('AEDICES.chat.rollDetail.interactionScore.title');
        
        // Card balance icons
        const cardBalance_positiveIcon = 'systems/acariaempire/resources/interaction/tokens/card_balance_positive.png';
        const cardBalance_negativeIcon = 'systems/acariaempire/resources/interaction/tokens/card_balance_negative.png';
        context.roll.balanceIcons = [];
        const balanceIcon = context.roll.cardBalance > 0 ? cardBalance_positiveIcon : cardBalance_negativeIcon;
        for( let i = 0; i < Math.abs(context.roll.cardBalance); i++ ) {
            context.roll.balanceIcons.push(balanceIcon);
        }
        return context;
    }

}


export class AEBasicHud extends Application {

    #frame = {
        left: 0,
        bottom: 0
    }

    #move = {
        moving: false,
        currentPos: { x: 0, y: 0 },
        listener: e => this._onMouseMove(e),
        wholeView : null
    };

    constructor(options = {}) {
        super(options);

        this.#registerSettings();

        const hudSettings = this.hudSettings;
        this.#frame.left = this.#getGameSettingValue(hudSettings.left.setting);
        this.#frame.bottom = this.#getGameSettingValue(hudSettings.bottom.setting);
    }

    /** 
     * Settings for positing storage inside game
     * If not set, no storage with be process and each F5 will reset HUD position
     * @abstract
     */
    get hudSettings() {
        return {
            left: { setting: "", default: 0 },
            bottom: { setting: "", default: 0 }
        }
    }

    #registerSettings() {
        const hudSettings = this.hudSettings;
        [hudSettings.left, hudSettings.bottom]
            .filter( hudSetting => hudSetting.setting != "")
            .forEach( hudSetting => {
                game.settings.register("ae-dices", hudSetting.setting, {
                    scope: "client",
                    config: false,
                    default: hudSetting.default,
                    type: Number,
                    config: false
                });
            });
    }

    #getGameSettingValue(settingKey) {
        if( settingKey === "" ) { return 0; }
        return game.settings.get('ae-dices', settingKey);
    }

    async getData() {
        let data = {
            frameInnerStyle : `bottom: ${this.#frame.bottom}px; left: ${this.#frame.left}px;`,
        };

        return data;
    }

    /*---------------------------------------------------------------------
      Moving panel
    ---------------------------------------------------------------------- */

    /** @override */
    _onDragStart(event) {
        event.preventDefault();
        this.#move.currentPos.x = event.clientX;
        this.#move.currentPos.y = event.clientY;
        this.#move.moving = true;
        this.#move.wholeView = event.currentTarget.parentElement.parentElement;

        this.#move.wholeView.addEventListener("mousemove", this.#move.listener );
        this.#move.wholeView.addEventListener("mouseup", e => this.moveHasEnded(e), {once: true});

        this.render();
    }

    /** @override */
    _canDragStart(selector) {
        return true;
    }

    async _onMouseMove(event) {

        if( !this.#move.moving ) { return; }

        event.preventDefault();
        const movement = {
            x: event.clientX - this.#move.currentPos.x,
            y: event.clientY - this.#move.currentPos.y
        };
        this.#move.currentPos.x = event.clientX;
        this.#move.currentPos.y = event.clientY;


        this.#frame.left += movement.x;
        this.#frame.bottom -= movement.y;
        this.render()
    }

    async moveHasEnded(event) {
        event.preventDefault();

        this.#move.moving = false;
        this.#move.wholeView.removeEventListener("mousemove", this.#move.listener);

        const hudSettings = this.hudSettings;
        if( hudSettings.left.setting != "" ) {
            await game.settings.set("ae-dices", hudSettings.left.setting, this.#frame.left );
        }
        if( hudSettings.bottom.setting != "" ) {
            await game.settings.set("ae-dices", hudSettings.bottom.setting, this.#frame.bottom );
        }
    }

}


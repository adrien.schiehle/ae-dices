import { AEBasicHud } from "./BasicHud.js";

const PORTRAIT_DEFAULT = "icons/svg/mystery-man.svg";
const WEAPON_IMG_DEFAULT = "systems/acariaempire/resources/items/weapon/barehanded.webp";

const circleTokenPath = (type, quantity) => {
    const icon = 'modules/ae-dices/resources/tokens/circle_' + type + '.png';
    return {
        quantity: quantity == 0 ? '' : '' + quantity,
        icon: icon,
        css: quantity == 0 ? 'transparent' : '',
        action: 'arcane'
    };
};

const spiritTokenPath = (type, quantity) => {
    const icon = 'modules/ae-dices/resources/tokens/spirit_' + type + '.png';
    let action;
    if( type == 'amount' ) { action = 'communion' }
    else { action = 'spiritRelease'; }

    return {
        quantity: '' + quantity,
        icon: icon,
        css: '',
        action: action
    };
};

const melodyTokenPath = (type, quantity) => {
    const icon = 'modules/ae-dices/resources/tokens/melody_' + type + '.png';
    return {
        quantity: '' + quantity,
        icon: icon,
        css: '',
        action: 'melody'
    };
};



const classicTokenPath = (type, quantity, alwaysDisplayed=false) => {

    const suffix = quantity > 0 ? '_positive.png': '_negative.png';
    const icon = 'modules/ae-dices/resources/tokens/' + type + suffix;
    return {
        quantity: '' + Math.abs(quantity),
        icon: icon,
        css: '',
        action: type, 
        alwaysDisplayed: alwaysDisplayed
    };
}

export class AETokensHud extends AEBasicHud {

    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            id: "ae-tokens-hud",
            template: 'modules/ae-dices/resources/tokens-hud.hbs',
            popOut: false,
            dragDrop: [{ dragSelector: ".select-icon"}]
        });
    }

    #selection = { health:0, endurance: 0, advantage : 0, minorEvents : 0, power : 0, complication : 0 };
    #troubleData = { display: false, img: null };
    #selectedActor = null;
    #currentTrouble = null;
    #spiritTokens = [];
    #circleTokens = [];
    #melodyTokens = [];
    #portrait = PORTRAIT_DEFAULT;
    #weaponImg = WEAPON_IMG_DEFAULT;
    #colorClass = "";

    constructor(options = {}) {
        super(options);
        console.log('AE-TokenHud | Initializing...');

        this.#selectedActor = game.aesystem.actorTools.defaultToken;
        this.#updateTokensAndCards();
    }

    /** @override */
    get hudSettings() {
        return {
            left: { setting: "panelLeft", default: 800 },
            bottom: { setting: "panelBottom", default: 400 }
        }
    }

    get classicTokens() {
        const allTokens = [
            classicTokenPath( 'health', this.#selection.health, true ),
            classicTokenPath( 'energy', this.#selection.endurance, true ),
            classicTokenPath( 'power', this.#selection.power, true ),
            classicTokenPath( 'advantage', this.#selection.advantage ),
            classicTokenPath( 'minorEvents', this.#selection.minorEvents ),
            classicTokenPath( 'skulls', this.#selection.complication )
        ];
        return allTokens.filter( t => t.alwaysDisplayed || t.quantity != '0' );
    }

    get allTokens() {
        return this.classicTokens
                .concat( this.#circleTokens )
                .concat( this.#spiritTokens )
                .concat( this.#melodyTokens );
    }

    /** @override */
    async getData() {
        const data = await super.getData();
        data.colorClass = this.#colorClass;
        data.piles = this.allTokens;
        data.portrait = this.#portrait;
        data.weaponImg = this.#weaponImg;
        data.trouble = this.#troubleData;

        return data;
    }

    activateListeners(html) {
        super.activateListeners(html);

        const helpers = game.modules.get("world-anvil").helpers;
        helpers.substituteShortcutsAndHandleClicks(html, {classes: ['.wa-tooltip', '.wa-frame'], changeLabels: false});

        // Flip event cards
        html.find(".portrait").click(event => this.#onLeftClickCenterOnActor(event));
        html.find(".portrait").contextmenu(event => this.#onRightClickDisplayActor(event));

        html.find(".token").click(event => this.#onLeftClickOnSlot(event));
        html.find(".token").contextmenu(event => this.#onRightClickOnSlot(event));

        html.find(".weapon").click(event => this.#onLeftClickAttack(event));
        html.find(".weapon").contextmenu(event => this.#onRightClickDisplayEquipment(event));

        html.find(".trouble").click(event => this.#onClickDisplayTrouble(event));

        html.find(".dice").click(event => this.#onLeftClickTriggerRoll(event));
        html.find(".dice").contextmenu(event => this.#onRightClickDisplayHelp(event));
    }

    /** Center camera on actor */
    async #onLeftClickCenterOnActor(event) {
        event.preventDefault();
        if( !this.#selectedActor ) { return; }

        const tokens = this.#selectedActor.token ? [this.#selectedActor.token] : this.#selectedActor.getActiveTokens();
        if( tokens.length > 0 ) {
            canvas.animatePan({x: tokens[0].document.x, y: tokens[0].document.y});
        }
    }
    
    /** Actor sheet display */
    async #onRightClickDisplayActor(event) {
        event.preventDefault();
        this.#selectedActor?.sheet.render(true);
    }

    /** InteractionGUI display with physical attack preset */
    async #onLeftClickAttack(event) {
        event.preventDefault();
        game.aesystem.attackWithMainWeapon();
    }

    /** Actor sheet display on equipment panel */
    async #onRightClickDisplayEquipment(event) {
        event.preventDefault();
        this.#selectedActor?.sheet.openEquipmentTab(true);
    }

    /** When clicking on a token */
    async #onLeftClickOnSlot(event) {

        event.preventDefault();
        if( !this.#selectedActor ) { return; }

        const a = event.currentTarget;
        const action = a.dataset.action;

        game.aesystem.actorTools.selectToken({
            tokenId: this.#selectedActor.token?.id,
            actorId: this.#selectedActor.id
        });
        
        if( action == 'arcane') {
            game.aesystem.triggerRollPanel({ battle: 'arcane', battleSubchoice: 'circle' });
        } else if( action == 'communion' ) {
            game.aesystem.triggerRollPanel({ battle: 'divine', battleSubchoice: 'communion' });
        } else if( action == 'spiritRelease' ) {
            game.aesystem.triggerRollPanel({ battle: 'divine', battleSubchoice: 'miracle' });
        } else if( action == 'melody' ) {
            game.aesystem.triggerRollPanel({ battle: 'songs', battleSubchoice: 'play' });

        } else if( action == 'health' ) {
            this.#selectedActor?.healDamage(1, {sendMessage: false});
        } else if( action == 'energy' ) {
            this.#selectedActor?.healFatigue(1, {sendMessage: false});
        } else if( action == 'power' ) {
            this.#selectedActor?.setPower( this.#selectedActor?.power + 1 );
        } else if( action == 'advantage' ) {
            this.#selectedActor?.setAdvantage( this.#selectedActor?.advantage + 1 );
        } else if( action == 'minorEvents' ) {
            this.#selectedActor?.setMinorEvents( this.#selectedActor?.minorEvents + 1 );
        } else if( action == 'complication' ) {
            this.#selectedActor?.setComplication( this.#selectedActor?.complication + 1 );
        }
    }

    /** When clicking on a token */
    async #onRightClickOnSlot(event) {

        event.preventDefault();
        if( !this.#selectedActor ) { return; }

        const a = event.currentTarget;
        const action = a.dataset.action;

        game.aesystem.actorTools.selectToken({
            tokenId: this.#selectedActor.token?.id,
            actorId: this.#selectedActor.id
        });
        
        if( action == 'arcane') {
            this.#selectedActor?.sheet.openArcaneTab(true);
        } else if( action == 'melody' ) {
            this.#selectedActor?.sheet.openMelodyTab(true);
        } else if( action == 'communion' || action == 'spiritRelease' ) {
            const cardsSheet = this.#selectedActor?.cards.pile.stack.sheet;
            if( cardsSheet ) {
                cardsSheet.selectFirstCard();
                cardsSheet.render(true);
            }

        } else if( action == 'health' ) {
            this.#selectedActor?.inflictDamage(1, {sendMessage: false});
        } else if( action == 'energy' ) {
            this.#selectedActor?.inflictFatigue(1, {sendMessage: false});
        } else if( action == 'power' ) {
            this.#selectedActor?.setPower( this.#selectedActor?.power - 1 );
        } else if( action == 'advantage' ) {
            this.#selectedActor?.setAdvantage( this.#selectedActor?.advantage - 1 );
        } else if( action == 'minorEvents' ) {
            this.#selectedActor?.setMinorEvents( this.#selectedActor?.minorEvents - 1 );
        } else if( action == 'complication' ) {
            this.#selectedActor?.setComplication( this.#selectedActor?.complication - 1 );
        }
    }

    async #onClickDisplayTrouble(event) {
        event.preventDefault();
        this.#selectedActor?.cards.renderRevealedCards(this.#currentTrouble?.card.id);
    }

    /** Left click on dice : Open Interaction Panel */
    async #onLeftClickTriggerRoll(event) {
        event.preventDefault();
        game.aesystem.triggerRollPanel();
    }
    /** Right click on dice : Open Help */
    async #onRightClickDisplayHelp(event) {
        event.preventDefault();
        const waHelpers = game.modules.get("world-anvil").helpers;
        await waHelpers.displayWAPage(undefined, "interaction.system.resolve", "section.summary");
    }

    changeCurrentSelection(actor) {
        if(!actor) return;
        
        this.#selectedActor = actor ?? game.aesystem.actorTools.defaultToken;
        const changed = this.#updateTokensAndCards();
        if( changed ) { this.render(); }
    }

    characterTokensHasBeenUpdated(actor) {
        if( actor != this.#selectedActor ) { return; }

        const changed = this.#updateTokensAndCards();
        if( changed ) { this.render(); }
    }

    thereAreCardsMovements(cards) {
        if( !this.#selectedActor ) { return; }

        // Does this stack concerns our current selection ?
        const cardData = this.#selectedActor.cards;
        const playerStacks = [cardData.hand, cardData.pile];
        if( !playerStacks.find( s => s.stack.id === cards?.id ) ) { return; }

        const changed = this.#updateTokensAndCards();
        if( changed ) { this.render(); }
    }

    /** 
     * Update data for rendering.
     * @returns True if data changed (=> Will need to be rendered again)
     */
    #updateTokensAndCards() {
        let hasChanged = false;
        hasChanged = this.#updateTokenData() || hasChanged;
        hasChanged = this.#updatePortraitData() || hasChanged;
        hasChanged = this.#updateWeaponData() || hasChanged;
        hasChanged = this.#updateCircleData()  || hasChanged;
        hasChanged = this.#updateSpiritData() || hasChanged;
        hasChanged = this.#updateMelodyData() || hasChanged;
        hasChanged = this.#updateTroubleData()  || hasChanged;
        hasChanged = this.#updateFrameColor() || hasChanged;
        
        return hasChanged;
    }



    /** @returns TRUE if something changed */
    #updateTokenData() {

        const healthOld = this.#selection.health;
        const enduranceOld = this.#selection.endurance;
        const advantageOld = this.#selection.advantage;
        const minorEventsOld = this.#selection.minorEvents;
        const powerOld = this.#selection.power;
        const complicationOld = this.#selection.complication;

        const noActor = (this.#selectedActor == null);
        this.#selection = noActor ? { health:0, endurance:0, advantage: 0, minorEvents: 0, power: 0, complication: 0 }
                                : { health: this.#selectedActor.health, 
                                    endurance: this.#selectedActor.currentFatigue, 
                                    advantage: this.#selectedActor.advantage, 
                                    minorEvents: this.#selectedActor.minorEvents, 
                                    power: this.#selectedActor.power,
                                    complication: this.#selectedActor.complication
                                  };

        return healthOld != this.#selection.health || enduranceOld != this.#selection.endurance 
            || advantageOld != this.#selection.advantage || minorEventsOld != this.#selection.minorEvents 
            || powerOld != this.#selection.power || complicationOld != this.#selection.complication;
    }

    /** @returns {boolean} TRUE if something changed */
    #updatePortraitData() {
        const oldPortrait =  this.#portrait;
        this.#portrait = this.#selectedActor?.img ?? PORTRAIT_DEFAULT ;
        return oldPortrait != this.#portrait;
    }

    /** @returns {boolean} TRUE if something changed */
    #updateWeaponData() {
        const oldWeaponImg = this.#weaponImg;
        this.#weaponImg = this.#selectedActor?.equipment.mainWeapon?.item?.img ?? WEAPON_IMG_DEFAULT;
        return oldWeaponImg != this.#weaponImg;
    }

    /** @returns {boolean} TRUE if something changed */
    #updateCircleData() {

        const oldCircleTokens = foundry.utils.duplicate(this.#circleTokens);
        const currentCircleCaracs = this.#selectedActor?.arcane.circleCaracteristics ?? [];
        const fragility = this.#selectedActor?.arcane.circleFragility ?? 0;

        this.#circleTokens = currentCircleCaracs.map( c => circleTokenPath( c.name, c.level ) );
        if( fragility > 0 ) {
            this.#circleTokens.push( circleTokenPath( 'fragility', fragility ) );
        }

        // Check if something changed
        if( oldCircleTokens.length != this.#circleTokens.length ) { return true; }
        for( let i = 0; i < oldCircleTokens.length; i++ ) {
            if( oldCircleTokens[i] != this.#circleTokens[i] ) {
                return true;
            }
        }
        return false;
    }

    /** @returns {boolean} TRUE if something changed */
    #updateSpiritData() {

        const oldSpiritTokens = foundry.utils.duplicate(this.#spiritTokens);
        const communion = this.#selectedActor?.divine.communion;

        this.#spiritTokens = [];
        const nbSprites = communion?.calledSpirits ?? 0;
        if( nbSprites > 0 ) {
            this.#spiritTokens.push( spiritTokenPath('amount', nbSprites) );
            if( communion?.dissatisfaction > 0 ) {
                this.#spiritTokens.push( spiritTokenPath('angry', communion.dissatisfaction) );
            }
        }

        // Check if something changed
        if( oldSpiritTokens.length != this.#spiritTokens.length ) { return true; }
        for( let i = 0; i < oldSpiritTokens.length; i++ ) {
            if( oldSpiritTokens[i] != this.#spiritTokens[i] ) {
                return true;
            }
        }
        return false;
    }

    /** @returns {boolean} TRUE if something changed */
    #updateMelodyData() {

        const oldMelodyTokens = foundry.utils.duplicate(this.#melodyTokens);
        
        this.#melodyTokens = [];
        const perf = this.#selectedActor?.songs.currentPerformance;
        if( perf ) {
            this.#melodyTokens.push( melodyTokenPath(perf.melodyKey, perf.level) );

            if( perf.dissonance > 0 ) {
                this.#melodyTokens.push( melodyTokenPath('dissonance', perf.dissonance) );
            }
        }

        // Check if something changed
        if( oldMelodyTokens.length != this.#melodyTokens.length ) { return true; }
        for( let i = 0; i < oldMelodyTokens.length; i++ ) {
            if( oldMelodyTokens[i] != this.#melodyTokens[i] ) {
                return true;
            }
        }
        return false;
    }

    /** 
     * Display on icon on the left of the GUI with the current trouble of your character
     * @returns {boolean} TRUE if something changed 
     */
    #updateTroubleData() {
        
        const oldTroubleData = foundry.utils.duplicate(this.#troubleData);
        const trouble = this.#selectedActor?.cards.troubles.find( t => t.isPositive || t.isNegative ) ?? null;
        this.#troubleData = {
            display: trouble != null,
            img: trouble?.isPositive ? trouble?.effect.positive.icon : trouble?.effect.negative.icon
        };
        this.#currentTrouble = trouble;

        return JSON.stringify(oldTroubleData) != JSON.stringify(this.#troubleData);
    }



    /** @returns {boolean} TRUE if something changed */
    #updateFrameColor() {

        const oldFrameCss = this.#colorClass;
        if( this.#selectedActor ) {
            const playersOwningIt = this.#selectedActor.playersOwningIt;
            const playerSide = playersOwningIt.length > 0;
            const ownedByUser = playersOwningIt.findIndex( u => u.id === game.user.id ) != -1;

            if( game.user.isGM ) {
                if( playerSide ) {
                    this.#colorClass = 'blue';
                }
                
            } else if( !playerSide ) {
                if( this.#selectedActor.isMonster ) {
                    this.#colorClass = 'red';
                }
            } else if( !ownedByUser ) {
                this.#colorClass = 'blue';
            }
        }
        return oldFrameCss != this.#colorClass;
    }

}


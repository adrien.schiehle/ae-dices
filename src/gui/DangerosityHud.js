import { AEBasicHud } from "./BasicHud.js";


export class AEDangerosityHud extends AEBasicHud {

    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            id: "ae-dangerosity-hud",
            template: 'modules/ae-dices/resources/dangerosity-hud.hbs',
            popOut: false,
            dragDrop: [{ dragSelector: ".select-icon"}]
        });
    }

    #scene;
    constructor(options = {}) {
        super(options);
        console.log('AE-DangerosityHud | Initializing...');
        this.#scene = game.canvas.scene;
    }

    /** @override */
    get hudSettings() {
        return {
            left: { setting: "dangerosityPanelLeft", default: 35 },
            bottom: { setting: "dangerosityPanelBottom", default: 500 }
        }
    }

    /** @override */
    async getData() {
        const data = await super.getData();
        data.userClass = game.user.isGM ? "asGM" : "asPlayer";
        data.dangerLevel = this.#scene?.getFlag("acariaempire", "dangerLevel") ?? 0;
        return data;
    }

    /** @override */
    activateListeners(html) {
        super.activateListeners(html);

        html.find(".danger-level.asGM").click(event => this.#onClickUpdateDangerLevel(event, 1));
        html.find(".danger-level.asGM").contextmenu(event => this.#onClickUpdateDangerLevel(event, -1));
    }

    async #onClickUpdateDangerLevel(event, offset) {
        event.preventDefault();
        if( this.#scene && game.user.isGM ) {
            const currentVal = this.#scene.getFlag("acariaempire", "dangerLevel") ?? 0;
            const newVal = Math.max(0, currentVal + offset);
            return this.#scene.setFlag("acariaempire", "dangerLevel", newVal);
        }
    }

    changeCurrentScene() {
        this.#scene = game.canvas.scene;
        this.render();
    }

    aSceneHasBeenUpdated(scene) {
        if( scene.id === this.#scene?.id ) {
            this.render();
        }
    }

}

